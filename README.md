# Surrogate Model Example

### 1. Data

Storing the (public) Allstate Claims Severity Kaggle dataset. 
This data is not currently being used in the surrogate model building process, but is available in case needed in future iterations. 

### 2. Scripts

Building a surrogate model using the freMTPL2freq data set from the CASdatasets R package.

We compare the following models:

1. a "base" generalized linear model (GLM); 
called "glm_base"

2. a "base" xgboost model; 
called "xgb_base"

3. a GLM surrogate model trained on the xgboost predictions using all predictors; 
called "glm_surr"

### 3. Results

Folders with dates correspond to different iterations of the models that were trained and compared.

#### Model rds objects

.rds objects that can assist in avoiding training time for the four separate models.

#### csv results

.csv files that compare the MAE, RMSE, and R-squared for each of the three models.


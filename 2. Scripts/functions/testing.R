
#'
#' Calculate the average of the cross-validation results
#' 
#' @param df The output from cross-validation
#' 

calculate_cv_averages <- function(df) {
  
  bind_rows(
    df %>% mutate(Fold = as.character(Fold)),
    tibble(Fold = 'Average') %>%
      bind_cols(
        df %>% summarize_at(names(df)[-1], mean)
      )
  )
  
}

#' 
#' Combine the results from all models
#' 
#' @param results The testing results from one of glm_base, xgb_base, glm_surr, or glm_surr_ovb
#' @param model_name One of "glm_base", "xgb_base", "glm_surr", or "glm_surr_ovb"
#' 

combine_testing <- function(results, model_name) {
  
  results %>%
    pivot_longer(
      cols = mean_absolute_error:root_mean_squared_error, 
      names_to = 'Metric', 
      values_to = paste('Value', model_name, sep = '_')
    )
  
}
